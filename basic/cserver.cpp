#include<cserver.hpp>
namespace configure
{

    cserver &cserver::init()
    {
        server_socket = socket(AF_INET, SOCK_STREAM, 0);
        if (server_socket < 0)
            throw std::runtime_error("socket error");
        memset(&serv_addr, 0, sizeof(serv_addr));
        memset(server_conn_socket, -1, sizeof(server_conn_socket[0]) * CONNECTION_SOCKET_COUNT);
        return *this;
    }
    cserver &cserver::bind_addr(sockaddr_in& serv_addr)
    {
        if (bind(server_socket, (sockaddr *)&serv_addr, sizeof(sockaddr)))
            throw std::runtime_error("bind error " + std::to_string(errno));
        return *this;
    }
    cserver &cserver::listen_conn()
    {
        if(listen(server_socket,CONNECTION_SOCKET_COUNT) == -1)
            throw std::runtime_error("listen error");
        return *this;
    }
    cserver &cserver::accept_conn(unsigned &i) 
    {
        int * ptr = server_conn_socket;
        i = 0;
        while(*ptr != -1)
        {
            ++i;
            if(i > CONNECTION_SOCKET_COUNT)
            {
                throw std::runtime_error("Max socket count");
            }
        }
        socklen_t len = 0;
        ptr[i] = accept(server_socket,(sockaddr*)(client_addr + i),&len);
        if(ptr[i] < 0)
            throw std::runtime_error("Accept error " + std::to_string(errno));
        return *this;
    }
    
    cserver & cserver::recv_all(void* ptr,size_t len,const unsigned i) 
    {
        configure::recv_all(server_conn_socket[i],ptr,len);
        return *this;
    }
    cserver & cserver::send_all(void* ptr,size_t len,const unsigned i) 
    {
        configure::send_all(server_conn_socket[i],ptr,len);
        return *this;
    }
    void cserver::close_sr()
    {
        close(server_socket);
        for(unsigned i = 0;i != CONNECTION_SOCKET_COUNT;++i)
        {
            if(server_conn_socket[i] != -1)
            {
                close(server_conn_socket[i]);
                server_conn_socket[i] = -1;
            }
        }
    }
    cserver & cserver::close_conn(const unsigned i)
    {
        close(server_conn_socket[i]);
        server_conn_socket[i] = -1; 
        return *this;
    }
    int cserver::get_conn(const unsigned i)
    {
        return server_conn_socket[i];
    }
    sockaddr_in& cserver::get_sockaddr(const unsigned i)
    {
        return client_addr[i];
    }

}