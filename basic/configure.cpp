#include<configure.hpp>
namespace configure    
{

    void recv_all(int sock,void*ptr,size_t len)
    {
        while(len != 0)
        {
        size_t b = recv(sock,ptr,len,0) ;
        if(b < 0)
            throw std::runtime_error("send error" + std::to_string(errno));
        if(b == 0)
            break;
        len -= b;
        }
    }
    void send_all(int sock,void*ptr,size_t len)
    {
        while(len != 0)
        {
        size_t b = send(sock,ptr,len,0) ;
        if(b < 0)
            throw std::runtime_error("send error" + std::to_string(errno));
        if(b == 0)
            break;
        len -= b;
        }
    }
    std::unique_ptr<sockaddr_in>  sock_addr_creator(char * name,uint16_t port)
    {
    hostent *server = gethostbyname(name);
    auto ptr = std::make_unique<sockaddr_in>();
    memset(ptr.get(),0,sizeof(sockaddr));
    ptr->sin_family = AF_INET;
    memcpy((char *)&ptr->sin_addr.s_addr,(char *)server->h_addr, server->h_length);
    ptr->sin_port = htons(port);
    return ptr;
    }
}