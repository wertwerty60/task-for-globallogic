#pragma once
#include<cinttypes>
#include<dirent.h>
#include<stdexcept>
#include <cerrno>
#include<memory>
#include <arpa/inet.h>
#include <netdb.h>
#include<cstring>
namespace configure    
{
    constexpr uint8_t CONNECTION_SOCKET_COUNT = 128;
    void recv_all(int sock,void*ptr,const size_t len);
    void send_all(int sock,void*ptr,const size_t len);
    std::unique_ptr<sockaddr_in>  sock_addr_creator(char * name,uint16_t port);
}