#pragma once
#include <sys/socket.h>
#include <configure.hpp>
#include <netinet/in.h>
#include <stdexcept>
#include <string.h>
#include <errno.h>
#include <unistd.h>
namespace configure
{
    struct cserver
    {
        cserver & init();
        cserver & bind_addr(sockaddr_in& serv_addr);
        cserver & listen_conn() ;
        cserver & accept_conn(unsigned &i);
        cserver & recv_all(void* ptr,size_t len,const unsigned i) ;
        cserver & send_all(void* ptr,size_t len,const unsigned i) ;
        void close_sr();
        cserver & close_conn(const unsigned i);
        int get_conn(const unsigned i);
        sockaddr_in& get_sockaddr(const unsigned i);
        private:
        int server_conn_socket[configure::CONNECTION_SOCKET_COUNT];
        sockaddr_in client_addr[configure::CONNECTION_SOCKET_COUNT];
        int server_socket;
        sockaddr_in serv_addr;
    };
}