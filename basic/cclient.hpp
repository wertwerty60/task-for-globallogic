#pragma once
#include <sys/socket.h>
#include <configure.hpp>
#include <netinet/in.h>
#include <stdexcept>
#include <string.h>
#include <unistd.h>
#include <cerrno>
namespace configure
{
    
    struct cclient
    {
        cclient &init();
        cclient &connect_addr(sockaddr_in& serv_addr) ;
        cclient &recv_all(void* ptr,const size_t len);
        cclient &send_all(void* ptr,const size_t len);
        void close_cl() const;
        private:
        int client_socket;
        socklen_t len;
    };

}
