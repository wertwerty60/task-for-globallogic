#include<cclient.hpp>
namespace configure
{
        cclient &cclient::init()
    {
        client_socket = socket(AF_INET, SOCK_STREAM, 0);
        if (client_socket <  0)
            throw std::runtime_error("socket error "+ std::to_string(errno));
        return *this;
    }
    cclient &cclient::connect_addr(sockaddr_in& serv_addr)
    {
        if(connect(client_socket, (sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
            throw std::runtime_error("connect error " + std::to_string(errno));
        return *this;
    }
    cclient & cclient::recv_all(void* ptr,size_t len)
    {
        configure::recv_all(client_socket,ptr,len);
        return *this;
    }
    cclient & cclient::send_all(void* ptr,size_t len)
    {
        configure::send_all(client_socket,ptr,len);
        return *this;
    }
    void cclient::close_cl() const
    {
        close(client_socket);

    }
}