#pragma once
#include <unistd.h>
#include <fcntl.h>
#include <stdexcept>
namespace tools
{
    class file
    {
        int file;

    public:
        void init(char *name, int fl);
        int read_all(char *buf, size_t size);
        int write_all(char *buf, size_t size);
        size_t size() const;
        void close_f() const;
    };
}
