    #include<tool.hpp>
        std::string get_dirs_names(const char *n)
    {
        std::string str;
        DIR *d = opendir(n);
        dirent *ptr;
        if (d == NULL)
        {
            throw std::runtime_error("Dir error");
        }
        while ((ptr = readdir(d)) != NULL)
        {
            if (ptr->d_type == DT_REG)
            {
                str.append(ptr->d_name);
                str.push_back('\n');
            }
        }
        closedir(d);
        str.push_back((char)0);
        return str;
    }

    
