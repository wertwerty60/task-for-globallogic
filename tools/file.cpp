#include <file.hpp>
namespace tools
{
    void file::init(char *name, int fl)
    {
        file = open(name, fl);
        if (file < 0)
            throw std::runtime_error("open error");
    }

    int file::read_all(char *buf, size_t size)
    {
        while (size != 0)
        {
            int b = read(file, buf, size);
            if (b < 0)
            {
                throw std::runtime_error("read error");
            }
            if (b == 0)
            {
                return 0;
            }
            size -= b;
        }
        return 1;
    }

    int file::write_all(char *buf, size_t size)
    {
        while (size != 0)
        {
            int b = write(file, buf, size);
            if (b < 0)
            {
                throw std::runtime_error("read error");
            }
            if (b == 0)
            {
                return 0;
            }
            size -= b;
        }
        return 1;
    }

    size_t file::size() const
    {
        long cur = lseek64(file, 0, SEEK_CUR);
        long size = lseek64(file, 0, SEEK_END);
        lseek64(file, cur, SEEK_SET);
        return size;
    }

    void file::close_f() const
    {
        close(file);
    }
}